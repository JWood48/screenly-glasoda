package main

import (
//	"crypto/md5"
    "os/exec"
    "log"
	"fmt"
	"html/template"
	"io"
	"time"
	"net/http"
	"os"
	"path/filepath"
	"github.com/tkanos/gonfig"
    "math/rand"
	//"database/sql"
    //"fmt"
    //"time"
    //_ "github.com/mattn/go-sqlite3"
//	"strconv"
//	"time"
)

type Conf struct {
	WorkDir       string
	AssetDir     string
    ScreenlyDb    string
}

var config = Conf{}



func handleUploadedFile(uploadedFile string) {

    base := filepath.Base(uploadedFile);

    path, err := filepath.Abs(filepath.Dir(uploadedFile));


        if err != nil {
            panic(err)
        }


    log.Printf("BASE: ", base);
    log.Printf("FILE: ", path);

    convert(uploadedFile, config.WorkDir+"/convert/"+base+".png");

    //convertFile := config.WorkDir+"/convert/";

}

func convert(pdfFile string, pngFile string) {
	cmd := exec.Command("convert", pdfFile, pngFile)
	log.Printf("Running command and waiting for it to finish...")
	err := cmd.Run()
	log.Printf("Command finished with error: %v", err)
}


var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randSeq(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = letters[rand.Intn(len(letters))]
    }
    return string(b)
}

func insertAsset(assetPath string) {

    // INSERT INTO assets VALUES('9b76369f56e543439ba5b622677fa3c5','VelkommenTilbage_150_90.png','/home/pi/screenly_assets/9b76369f56e543439ba5b622677fa3c5',NULL,'2019-10-20 08:54:00','2019-11-19 09:54:00','10','image',1,0,0,0,0);

    base := filepath.Base(assetPath);

    assetDir, _ := filepath.Abs(filepath.Dir(config.AssetDir))

    timeNow := time.Now()



    aId := randSeq(32);
    aName := base
    aURI := assetDir +"/"+aId
    aFrom := timeNow.format("yyyy-MM-dd HH:mm:ss") // "2019-10-20 08:54:00"
    aTo := timeNow.Add(time.Day * 14).format("yyyy-MM-dd HH:mm:ss") //"2019-10-20 08:54:00"


    //copyFile(asset);

    sqlInsert := "INSERT INTO assets VALUES('"+aId+"','"+aName+"','"+aURI+"',NULL,'"+aFrom+"','"+aTo+"','10','image',1,0,0,0,0);";


    cmd := exec.Command("sqlite3", config.ScreenlyDb, sqlInsert)

	log.Printf("Running command and waiting for it to finish...")
	err := cmd.Run()
	log.Printf("Command finished with error: %v", err)


}

func copyFile(from string, to string) {

        fromFile, err := os.Open(from)

		if err != nil {
        			fmt.Println(err)
        			return
        		}

        defer fromFile.Close()

		toFile, err := os.OpenFile(to, os.O_WRONLY|os.O_CREATE, 0666)


		if err != nil {
        			fmt.Println(err)
        			return
        		}

defer toFile.Close()

		io.Copy(fromFile, toFile)

}

func upload(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		// GET
		t, _ := template.ParseFiles("upload.gtpl")

		t.Execute(w, nil)

	} else if r.Method == "POST" {
		// Post
		file, handler, err := r.FormFile("uploadfile")
		if err != nil {
			fmt.Println(err)
			return
		}
		defer file.Close()

        uploadFilePath := config.WorkDir+"/upload/"+handler.Filename
		fmt.Fprintf(w, "%v", handler.Header)
		f, err := os.OpenFile(uploadFilePath, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			fmt.Println(err)
			return
		}
		defer f.Close()

		io.Copy(f, file)

        //log.Printf("FILE: ", thepath);
        //log.Printf("FILE: ", uploadFilePath);
        //log.Printf("FILE: ", f.Name());

        handleUploadedFile(f.Name());




	} else {
		fmt.Println("Unknown HTTP " + r.Method + "  Method")
	}
}

func main() {

    jsonConfig := os.Args[1]

//    	configuration := Conf{}
    	err := gonfig.GetConf(jsonConfig, &config)

    	if err != nil {
    		log.Fatal(err)
    		return
    	}

    log.Printf("CONFIG: %+v", config)

    os.MkdirAll(config.WorkDir+"/upload", os.ModePerm)
    os.MkdirAll(config.WorkDir+"/convert", os.ModePerm)
    os.MkdirAll(config.AssetDir, os.ModePerm)


	http.HandleFunc("/upload", upload)
	http.ListenAndServe(":9090", nil) // setting listening port
}
