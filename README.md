#GLASODA SCREENLY HELPER



## Installing CloudStats agent:

## Turn off screen at night:

see article: https://www.screenly.io/blog/2017/07/02/how-to-automatically-turn-off-and-on-your-monitor-from-your-raspberry-pi/

Cron jobs
```
 # Turn monitor on
 0 7  * * 1-5 /usr/bin/vcgencmd display_power 1

 # Turn monitor off
 0 20 * * 1-5 /usr/bin/vcgencmd display_power 0

```

or 

```
# minute   hour   day of month     month   day of week       command
#  0-59    0-23   1-31             1-12    0-7 (0/7 is Sun) 
30         18     *                *       *                 /opt/vc/bin/tvservice -p ; /opt/vc/bin/tvservice -o
30         8      *                *       1-5               /opt/vc/bin/tvservice -p ; sudo chvt 6; sudo chvt 7

```